package dbs.guericke;

import uk.me.jstott.jcoord.LatLng;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.Properties;
import java.util.Collection;
import java.util.LinkedList;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * {@code Tell} is an interactive program through which the user can get
 * weather information for a location of her choice at a date of her choice.
 */
public class Tell {
    private final static String DB_URL
        = "jdbc:postgresql://localhost/guericke";
    private final static String DB_USER  = "erle";
    private final static String DB_PASSW = "halbkugeln";


    /**
     * Prompts the user for a location and a date and outputs weather
     * information accordingly.
     */
    public static void main (String[] args)
            throws SQLException, ClassNotFoundException, IOException {
        // Connect to the database
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASSW);
        Connection conn = DriverManager.getConnection(DB_URL, props);

        // Prompt for an arbitrary query
        BufferedReader stdin
            = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Query: ");
        String queryString = stdin.readLine();

        // Execute it
        Statement query         = conn.createStatement();
        ResultSet res           = query.executeQuery(queryString);
        ResultSetMetaData resMD = res.getMetaData();


        // Print the results in semi-tabular form
        int colCnt = resMD.getColumnCount();
        for (int i = 1; i <= colCnt; ++i) {
            System.out.printf(
                "%" + resMD.getColumnDisplaySize(i) + "s  ",
                resMD.getColumnLabel(i)
            );
        }
        System.out.println();
        while (res.next()) {
            for (int i = 1; i <= colCnt; ++i) {
                System.out.printf(
                    "%" + resMD.getColumnDisplaySize(i) + "s  ",
                    String.valueOf( res.getObject(i) )
                );
            }
            System.out.println();
        }

        res.close();
    }
}
