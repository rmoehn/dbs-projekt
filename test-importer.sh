#!/bin/bash

psql guericke -c 'TRUNCATE station CASCADE;'
psql guericke -c 'TRUNCATE measurement CASCADE;'
psql guericke -c 'TRUNCATE location CASCADE;'
psql guericke -c 'TRUNCATE plz CASCADE;'
psql guericke -c 'TRUNCATE distance CASCADE;'

cd data-importer
mvn package assembly:single
cd ..
cp -f data-importer/target/data-importer-1.0-SNAPSHOT-bin.tar.gz .
tar -xzf data-importer-1.0-SNAPSHOT-bin.tar.gz
cd data-importer-1.0-SNAPSHOT
java -jar data-importer-1.0-SNAPSHOT.jar \
    --dburl localhost/guericke \
    --locfile ../Daten/DE.tab  \
    --measfile ../Daten/wetterdaten_Wettermessung.csv \
    --statfile ../Daten/wetterdaten_Wetterstation.csv \
    --username erle \
    --password halbkugeln

cd ..

#psql guericke -f print_all.sql
