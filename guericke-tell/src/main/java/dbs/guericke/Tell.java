package dbs.guericke;

import uk.me.jstott.jcoord.LatLng;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

import java.util.Properties;
import java.util.Collection;
import java.util.LinkedList;

import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * {@code Tell} is an interactive program through which the user can get
 * weather information for a location of her choice at a date of her choice.
 */
public class Tell {
    private final static String DB_URL
        = "jdbc:postgresql://localhost/guericke";
    private final static String DB_USER  = "erle";
    private final static String DB_PASSW = "halbkugeln";


    /**
     * Prompts the user for a location and a date and outputs weather
     * information accordingly.
     */
    public static void main (String[] args)
            throws SQLException, ClassNotFoundException, IOException {
        // Connect to the database
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user", DB_USER);
        props.setProperty("password", DB_PASSW);
        Connection conn = DriverManager.getConnection(DB_URL, props);

        // Cache weather stations
        Statement statQuery = conn.createStatement();
        ResultSet statRes
            = statQuery.executeQuery(
                  "SELECT stat_id, name, lat, lon, alt FROM station"
              );
        Collection<Station> stations = new LinkedList<>();
        while (statRes.next()) {
            stations.add(
                new Station(
                    statRes.getInt(1),
                    statRes.getString(2),
                    statRes.getDouble(3),
                    statRes.getDouble(4),
                    statRes.getInt(5)
                )
            );
        }
        statRes.close();

        // Create a reader for user inputs
        BufferedReader stdin
            = new BufferedReader(new InputStreamReader(System.in));

        // Prompt for the location
        System.out.print("Ort: ");
        String locName = stdin.readLine();

        // Find matching names in the database
        Statement locQuery = conn.createStatement(
                                 ResultSet.TYPE_SCROLL_INSENSITIVE,
                                 ResultSet.CONCUR_READ_ONLY
                             );
        ResultSet locRes
            = locQuery.executeQuery(
                  "SELECT loc_id, name, lat, lon, plz_value"
                  + " FROM location, plz"
                  + " WHERE loc_id = plz_loc_id"
                  + "     AND name ILIKE '" + locName + "';"
              );

        // Exit if there are no locations with matching name
        if (!locRes.first()) {
            System.out.println(
                "Kein Ort mit dem Namen " + locName + " gefunden.");
            System.exit(0);
        }

        // If there is only one row, use it. Else prompt the user.
        int rowNr = 1;
        if (locRes.next()) {
            // Reset to position before first row
            locRes.first();
            locRes.previous();

            // Print locations found
            System.out.println("Folgende Orte wurden gefunden:");
            while (locRes.next()) {
                System.out.printf(
                    "%2d\t%s\t%s\n",
                    locRes.getRow(),
                    locRes.getString(5),
                    locRes.getString(2)
                );
            }

            // Get the user's choice
            System.out.print("Auswahl: ");
            rowNr = Integer.parseInt(stdin.readLine());
        }

        // Read information from chosen entry and close the query
        locRes.absolute(rowNr);
        Location location = new Location(
                                locRes.getInt(1),
                                locName,
                                locRes.getDouble(3),
                                locRes.getDouble(4),
                                new String[]{ locRes.getString(5) }
                            );
        locRes.close();

        // Find nearest station
        LatLng locPos       = location.pos();
        Station nearestStat = null;
        double minDist      = 2000.0; // Should be OK for Germany.
        for (Station stat : stations) {
            double dist = locPos.distance( stat.pos() );
            if (dist < minDist) {
                minDist = dist;
                nearestStat = stat;
            }
        }

        // Prompt the user for date
        System.out.print("Datum (JJJJ-MM-DD): ");
        String date = stdin.readLine();

        // Get the weather for the found-out station and date
        Statement measQuery = conn.createStatement();
        ResultSet measRes
            = measQuery.executeQuery(
                  "SELECT meas_stat_id, meas_date, quality, temp_min_5cm,"
                  + "     temp_min_2m, temp_avg_2m, temp_max_2m, rel_humid,"
                  + "     avg_wind_beauf, max_wind_speed, sun_duration,"
                  + "     avg_cloud_cov, precip, avg_air_pressure"
                  + " FROM measurement"
                  + " WHERE meas_stat_id = " + nearestStat.statId()
                  + "     AND meas_date = '" + date + "';"
              );

        // Create a measurement object if data exist
        if (!measRes.next()) {
            System.out.println(
                "Am " + date + " gibt es für " + locName + " keine Daten.");
            System.exit(0);
        }
        Measurement measurement
            = new Measurement(
                  measRes.getInt(1),
                  measRes.getDate(2),
                  measRes.getInt(3),
                  measRes.getFloat(4),
                  measRes.getFloat(5),
                  measRes.getFloat(6),
                  measRes.getFloat(7),
                  measRes.getFloat(8),
                  measRes.getInt(9),
                  measRes.getFloat(10),
                  measRes.getFloat(11),
                  measRes.getFloat(12),
                  measRes.getFloat(13),
                  measRes.getFloat(14)
              );
        measRes.close();

        // Print it!
        System.out.printf(
            "Die Wetterstation %s ist von %s %3f Kilometer weit entfernt."
            + " Am %s war das Wetter dort so:\n",
            nearestStat.name(),
            location.name(),
            minDist,
            date
        );
        System.out.printf(
            "%-50s%.1f\n%-50s%.1f\n%-50s%.1f\n%-50s%.1f\n%-50s%.1f\n%-50s%d\n"
            + "%-50s%.1f\n%-50s%.1f\n%-50s%.1f\n%-50s%.1f\n%-50s%.1f\n",
            "Temperaturminimum am Boden / °C",
            measurement.tempMin5cm(),
            "Temperaturminimum in Kopfhöhe / °C",
            measurement.tempMin2m(),
            "Durchschnittstemperatur in Kopfhöhe / °C",
            measurement.tempAvg2m(),
            "Temperaturmaximum in Kopfhöhe / °C",
            measurement.tempMax2m(),
            "relative Luftfeuchtigkeit / %",
            measurement.relHumid(),
            "durchschnittliche Windstärke (Beaufort)",
            measurement.avgWindBeauf(),
            "maximale Windgeschwindigkeit / m/s",
            measurement.maxWindSpeed(),
            "Sonnenscheindauer / h",
            measurement.sunDuration(),
            "durchschnittlicher Bedeckungsgrad / 1/8",
            measurement.avgCloudCov(),
            "Niederschlagshöhe / mm",
            measurement.precipitation(),
            "durchschnittlicher Luftdruck / hPa",
            measurement.avgAirPressure()
        );
    }
}
