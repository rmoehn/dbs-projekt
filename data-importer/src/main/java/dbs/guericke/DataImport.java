package dbs.guericke;

import java.io.File;
import java.sql.Date;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.sql.*;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import uk.me.jstott.jcoord.LatLng;

/**
 * {@code DataImport} imports weather and location data from special text
 * files into the specified database. The database must already contain the
 * tables to hold the data to be imported.
 */
public class DataImport {
    private static final int ERR_FILE = 1;

    private static final NumberFormat germanNumbers
        = NumberFormat.getInstance(Locale.GERMAN);
    /**
     * A strategy to turn an array of strings into a {@link Location} object.
     */
    private static final DatafileReader.LineToObjStrategy<Location>
        lineToLocStrat = new DatafileReader.LineToObjStrategy<Location>() {
            /**
            * Return a tab, by which the fields of the lines in the
            * CSV file about locations are terminated.
            */
            public char separator() { return '\t'; }

            /**
             * The array is supposed to consist of six fields. The
             * fields have information about one location as follows:
             *
             *     0  ID
             *     3  name
             *     4  latitude (north)
             *     5  longitude (east)
             *     7  Array of PLZs
             *
             * All other fields are discarded. If the latitude or longitude
             * field is empty, {@code null} is returned.
             */
            public Location lineToObj(String[] fields) {
                // Return null if latitude or longitude are empty
                if (fields[4].length() == 0 || fields[5].length() == 0) {
                    return null;
                }

                // Extract the PLZs
                StringTokenizer st = new StringTokenizer(fields[7],",");
                Collection<String> plzCol = new HashSet<>(30);
                while (st.hasMoreTokens()) {
                    plzCol.add(st.nextToken());
                }
                String[] plzs = plzCol.toArray(new String[0]);

                return new Location(
                        Integer.parseInt(fields[0]),
                        fields[3],
                        Double.parseDouble(fields[4]),
                        Double.parseDouble(fields[5]),
                        plzs
                );
            }
        };

    /**
     * A strategy to turn an array of strings into a {@link Station} object.
     */
    private static final DatafileReader.LineToObjStrategy<Station>
        lineToStatStrat = new DatafileReader.LineToObjStrategy<Station>() {
            /**
             * Return a semicolon, by which the fields of the lines in the
             * CSV file about weather stations are terminated.
             */
            public char separator() { return ';'; }

            /**
             * The array is supposed to consist of six fields . The
             * fields have information about one weather station as follows:
             *
             *     0  ID
             *     1  name
             *     2  latitude (north)
             *     3  longitude (east)
             *     4  altitude
             *     5  owner
             *
             * The {@code owner} field is ignored.
             */
            public Station lineToObj(String[] fields) {
                double lat;
                double lon;
                try {
                    lat = germanNumbers.parse(fields[2]).doubleValue();
                    lon = germanNumbers.parse(fields[3]).doubleValue();
                }
                catch (ParseException e) {
                    throw new IllegalArgumentException("Bad CSV.");
                }

                return new Station(
                           Integer.parseInt(fields[0]),
                           fields[1],
                           lat,
                           lon,
                           Integer.parseInt(fields[4])
                );
            }
        };

    /**
     * A strategy to turn an array of strings into a {@link Measurement}
     * object.
     */
    private static final DatafileReader.LineToObjStrategy<Measurement>
        lineToMeasStrat
            = new DatafileReader.LineToObjStrategy<Measurement>() {
            /**
            * Return a semicolon, by which the fields of the lines in the
            * CSV file about the measurements are terminated.
            */
            public char separator() { return ';'; }

            /**
             * The array is supposed to consist of fourteen fields . The
             * fields have information about one measurment as follows:
             *
             *     0  ID
             *     1  date
             *     2  quality
             *     3  min. Temp. 5cm
             *     4  min. Temp. 2m
             *     5  avg. Temp. 2m
             *     6  max. Temp. 2m
             *     7  relative humidity
             *     8  avg. wind force
             *     9  max. wind speed
             *     10 sun duration
             *     11 avg. cloud coverage
             *     12 precipitation
             *     13 avg. air pressure
             */

            public Measurement lineToObj(String[] fields) {
                float tempMin5cm;
                float tempMin2m;
                float tempAvg2m;
                float tempMax2m;
                float relHumid;
                float maxWindSpeed;
                float sunDuration;
                float avgCloudCov;
                float precipitation;
                float avgAirPressure;
                // ist hier vielleicht quatsch!?
                try {
                    tempMin5cm
                        = germanNumbers.parse(fields[3]).floatValue();
                    tempMin2m
                        = germanNumbers.parse(fields[4]).floatValue();
                    tempAvg2m
                        = germanNumbers.parse(fields[5]).floatValue();
                    tempMax2m
                        = germanNumbers.parse(fields[6]).floatValue();
                    relHumid
                        = germanNumbers.parse(fields[7]).floatValue();
                    maxWindSpeed
                        = germanNumbers.parse(fields[9]).floatValue();
                    sunDuration
                        = germanNumbers.parse(fields[10]).floatValue();
                    avgCloudCov
                        = germanNumbers.parse(fields[11]).floatValue();
                    precipitation
                        = germanNumbers.parse(fields[12]).floatValue();
                    avgAirPressure
                        = germanNumbers.parse(fields[13]).floatValue();
                }
                catch (ParseException e) {
                    throw new IllegalArgumentException("Bad CSV.");
                }

                return new Measurement(
                        Integer.parseInt(fields[0]),
                        Date.valueOf(fields[1]),
                        Integer.parseInt(fields[2]),
                        tempMin5cm,
                        tempMin2m,
                        tempAvg2m,
                        tempMax2m,
                        relHumid,
                        Integer.parseInt(fields[8]),
                        maxWindSpeed,
                        sunDuration,
                        avgCloudCov,
                        precipitation,
                        avgAirPressure
                );
            }
        };

    /*
     * This approach to command line processing is rather bad for three
     * reasons:
     *
     *  - The class variables have to be public in order for the processor to
     *    work.
     *
     *  - We declare command line options whereas they are not optional.
     *
     *  - The program's starting procedure is rather awkward.
     *
     *  However, this is a standalone program to be used in this project only
     *  and in scripts only. Thus simplicity outweighs badness.
     */
    @Option(name="--dburl", usage="the url of the database to be used, see"
            + " PostgreSQL JDBC Interface documentation, chapter 3",
            required=true)
    public String dburl;

    @Option(name="--username", usage="the database username", required=true)
    public String username;

    @Option(name="--password", usage="the password belonging to the username")
    public String password;

    @Option(name="--locfile", usage="the text file with location information"
            + " (OpenGeoDB)", required=true)
    public File locfile;

    @Option(name="--statfile", usage="the text file with information about"
            + " weather stations", required=true)
    public File statfile;

    @Option(name="--measfile", usage="the text file with weather"
            + " measurements", required=true)
    public File measfile;

    /**
     * Creates an instance of this class.
     */
    private DataImport() { }

    /**
     * This method just starts the command line processing.
     */
    public static void main (String[] args) throws ClassNotFoundException,
           SQLException {
        new DataImport().mainWithArgs(args);
    }

    /**
     * This method starts all necessary activities for importation. Parameters
     * are given through not-really-optional command line options.
     */
    private void mainWithArgs(String[] args) throws ClassNotFoundException,
           SQLException {
        // Process the command line
        CmdLineParser clParser = new CmdLineParser(this);
        try {
            clParser.parseArgument(args);
        }
        catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println(
                "This program should be called with all the arguments"
                + " (except --password sometimes) to be seen below."
            );
            clParser.printUsage(System.err);
            System.err.println();

            return;
        }

        // Make sure the specified files exist
        checkFile(locfile);
        checkFile(statfile);
        checkFile(measfile);

        // Load the JDBC driver
        Class.forName("org.postgresql.Driver");

        // Connect to the database
        Properties props = new Properties();
        props.setProperty("user", username);
        if (password != null) {
            props.setProperty("password", password);
        }
        Connection conn = DriverManager.getConnection(
                              "jdbc:postgresql://" + dburl,
                              props
                          );

        // Prepare statement for inserting weather stations
        PreparedStatement statStmt
                = conn.prepareStatement(
                "INSERT INTO station (stat_id, name, lat, lon, alt)"
                        + " VALUES (?, ?, ?, ?, ?);"
        );

        // Retrieve station information and insert into station table
        DatafileReader<Station> stations
                = new DatafileReader<>(statfile, lineToStatStrat);
        Station s;
        while ((s = stations.next()) != null) {
            System.out.println(s.toString());

            statStmt.setInt(    1, s.statId() );
            statStmt.setString( 2, s.name()   );
            statStmt.setDouble( 3, s.lat()    );
            statStmt.setDouble( 4, s.lon()    );
            statStmt.setInt(    5, s.alt()    );

            statStmt.executeUpdate();
        }

        // Close the statement for inserting weather stations
        statStmt.close();

        // Prepare statement for inserting locations
        PreparedStatement locStmt
            = conn.prepareStatement(
                "INSERT INTO location (loc_id, name, lat, lon)"
                    + "VALUES (?, ?, ?, ?);"
        );

        // Prepare statement for inserting postcodes
        PreparedStatement plzStmt
            = conn.prepareStatement(
                "INSERT INTO plz (plz_loc_id, plz_value)"
                    + "VALUES (?, ?);"
        );

        // Prepare statement for inserting distances
        PreparedStatement distStmt
            = conn.prepareStatement(
                "INSERT INTO distance (loc_id, stat_id, dist)"
                    + "VALUES (?, ?, ?);"
        );

        // Cache weather stations
        Statement statQuery = conn.createStatement();
        ResultSet statRes
                = statQuery.executeQuery(
                "SELECT stat_id, name, lat, lon, alt FROM station"
        );
        Collection<Station> stationcol = new LinkedList<>();
        while (statRes.next()) {
            stationcol.add(
                new Station(
                    statRes.getInt(1),
                    statRes.getString(2),
                    statRes.getDouble(3),
                    statRes.getDouble(4),
                    statRes.getInt(5)
                )
            );
        }
        statRes.close();

        // Retrieve location information and insert into location table
        DatafileReader<Location> locations
            = new DatafileReader<>(locfile, lineToLocStrat);
        Location l;
        while ((l = locations.next()) != null) {
            System.out.println(l.toString());

            // Insert the location
            locStmt.setInt(    1, l.locId());
            locStmt.setString( 2, l.name());
            locStmt.setDouble( 3, l.lat());
            locStmt.setDouble( 4, l.lon());

            locStmt.executeUpdate();

            // Insert all postcodes belonging to it
            for (String plz : l.plzs()) {
                plzStmt.setInt(    1, l.locId());
                plzStmt.setString( 2, plz);

                plzStmt.executeUpdate();
            }

            // Insert the distances between locations and stations
            LatLng lPos = l.pos();
            int lID     = l.locId();
            for (Station stat : stationcol) {
                distStmt.setInt(    1, lID                         );
                distStmt.setInt(    2, stat.statId()               );
                distStmt.setDouble( 3, lPos.distance( stat.pos() ) );

                distStmt.executeUpdate();
            }
        }

        // Close the statements for locations, distances and postcodes
        locStmt.close();
        plzStmt.close();
        distStmt.close();

        // Prepare statement for inserting measurements
        PreparedStatement measStmt
            = conn.prepareStatement(
                "INSERT INTO measurement (meas_stat_id, meas_date, quality, " +
                        "temp_min_5cm, temp_min_2m, temp_avg_2m, temp_max_2m, "
                        + "rel_humid, avg_wind_beauf, max_wind_speed, " +
                        "sun_duration, avg_cloud_cov, precip, " +
                        "avg_air_pressure)" + "VALUES (?, ?, ?, ?, ?, ?, ?, " +
                        "?, ?, ?, ?, ?, ?, ?);"
        );

        // Retrieve measurement information and insert into measurement table
        DatafileReader<Measurement> measurements = new
                DatafileReader<Measurement>(measfile, lineToMeasStrat);
        Measurement m;
        while ((m = measurements.next()) != null) {
            System.out.println(m.toString());

            measStmt.setInt(    1, m.measStatId());
            measStmt.setDate(   2, m.date());
            measStmt.setInt(    3, m.quality());
            measStmt.setFloat(  4, m.tempMin5cm());
            measStmt.setFloat(  5, m.tempMin2m());
            measStmt.setFloat(  6, m.tempAvg2m());
            measStmt.setFloat(  7, m.tempMax2m());
            measStmt.setFloat(  8, m.relHumid());
            measStmt.setInt(    9, m.avgWindBeauf());
            measStmt.setFloat( 10, m.maxWindSpeed());
            measStmt.setFloat( 11, m.sunDuration());
            measStmt.setFloat( 12, m.avgCloudCov());
            measStmt.setFloat( 13, m.precipitation());
            measStmt.setFloat( 14, m.avgAirPressure());

            measStmt.executeUpdate();
        }

        // Close the statement for inserting measurements
        measStmt.close();

        // Close the connection to the database
        conn.close();
    }

    /**
     * Stops the program if the specified {@code File} doesn't exist.
     */
    private static void checkFile(File file) {
        if (! file.exists()) {
            System.err.println(file.toString() + " does not exist.");
            System.exit(ERR_FILE);
        }
    }
}
