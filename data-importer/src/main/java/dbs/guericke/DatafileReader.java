package dbs.guericke;

import java.io.File;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Sort of an iterator that reads a text file containing information about
 * certain objects line by line and returns objects of type {@code T}.
 */
class DatafileReader<T> {
    private final CSVReader csvfile;
    private final LineToObjStrategy<T> strat;

    /**
     * Strategies to turn arrays of strings as returned by the CSV library
     * into useful objects.
     */
    interface LineToObjStrategy<U> {
        /**
         * Returns the separating char to be used by the CSV library.
         */
        char separator();

        /**
         * Creates an object of type {@code U} from the information provided
         * by {@code fields}.
         */
        U lineToObj(String[] fields);
    }

    /**
     * Creates a new instance of this iterator, reading from the specified
     * {@code file}.
     *
     * The first line of the file is discarded.      */
    public DatafileReader(File file, LineToObjStrategy<T> strat) {
        this.strat = strat;

        // Open our input file
        try {
            csvfile = new CSVReader(new FileReader(file), strat.separator());
        }
        catch (FileNotFoundException e) {
            throw new AssertionError(); // specified file has to exist
        }

        // Discard the first line
        try {
            csvfile.readNext();
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(
                "Something wrong with the provided weather station file?"
            );
        }
    }

    /**
     * Returns the next {@link Object} or {@code null} if there are no more.
     */
    public T next() {
        // Read a line
        String[] values = null;
        try {
            values = csvfile.readNext();
        }
        catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException(
                "Something wrong with the provided weather station file?"
            );
        }

        // Return if the reader is exhausted
        if (values == null) {
            return null;
        }

        // Return next non-empty object of type T
        T obj = strat.lineToObj(values);
        return obj == null ? next() : obj;
    }
}
