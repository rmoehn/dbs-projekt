WITH special_stat  AS (
    SELECT meas_stat_id 
    FROM measurement
    WHERE meas_date = '2012-07-01'
    ORDER BY avg_wind_beauf DESC
    LIMIT 1
)
SELECT plz.plz_value, location.name 
FROM location, distance, plz
WHERE   distance.stat_id in (SELECT * FROM special_stat)
    AND distance.loc_id  = location.loc_id
    AND plz.plz_loc_id   = location.loc_id
ORDER BY distance.dist ASC
LIMIT 1;
