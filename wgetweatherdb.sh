#! /bin/bash

# This script will download and unzip the weather database of the german weather
# service from http://dbup2date.uni-bayreuth.de/downloads/wetterdaten/ into the
# specified output directory. A wget Logfile is written to the specified log
# directory.

# Settings
OUTDIR="${PWD}/Daten"
LOGDIR="${PWD}/Daten"
SOURCEFILE=$(date +%Y-%m-%d)_wetterdaten_CSV.zip

# Download
if which wget 2>/dev/null >/dev/null; then
    wget -O ${OUTDIR}/wetterdaten.zip -o ${LOGDIR}/wget_weatherdb.log \
    http://dbup2date.uni-bayreuth.de/downloads/wetterdaten/$SOURCEFILE
    if [ $? != "0" ]; then
        echo "Download failed.";
        false;
        exit;
    fi
else
    echo "wget wasn't found!";
    false;
    exit;
fi

# Unzip
unzip -o ${OUTDIR}/wetterdaten.zip -d ${OUTDIR}

# Remove the zip-file
rm -f ${OUTDIR}/wetterdaten.zip

# Convert to UTF-8
iconv -f ISO88591 -t UTF-8 -o ${OUTDIR}/wetterdaten_Wetterstation.csv.temp ${OUTDIR}/wetterdaten_Wetterstation.csv
iconv -f US-ASCII -t UTF-8 -o ${OUTDIR}/wetterdaten_Wettermessung.csv.temp ${OUTDIR}/wetterdaten_Wettermessung.csv
rm ${OUTDIR}/wetterdaten_Wetterstation.csv
rm ${OUTDIR}/wetterdaten_Wettermessung.csv
mv ${OUTDIR}/wetterdaten_Wetterstation.csv.temp ${OUTDIR}/wetterdaten_Wetterstation.csv
mv ${OUTDIR}/wetterdaten_Wettermessung.csv.temp ${OUTDIR}/wetterdaten_Wettermessung.csv

# Change the Line of Berlin Dalem Station
sed -i '/10381;Berlin-Dahlem (FU)/ c\
10381;Berlin-Dahlem (FU);52,4582283;13,3100698;68;;' ${OUTDIR}/wetterdaten_Wetterstation.csv

# THE END
exit;
