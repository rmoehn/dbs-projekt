﻿CREATE TABLE location (
loc_id              int PRIMARY KEY,
name                varchar(80),
lat                 double precision  NOT NULL,
lon                 double precision  NOT NULL,
);

CREATE TABLE plz (
plz_loc_id          int REFERENCES location (loc_id),
plz_value           varchar(5),
PRIMARY KEY (plz_loc_id, plz_value)
);

CREATE TABLE station (
stat_id             int PRIMARY KEY,
name                varchar(80),
lat                 double precision  NOT NULL,
lon                 double precision  NOT NULL,
alt                 int
);

CREATE TABLE distance (
loc_id              int REFERENCES location (loc_id),
stat_id             int REFERENCES station (stat_id),
dist                double precision  NOT NULL,
PRIMARY KEY (loc_id, stat_id)
);

CREATE TABLE measurement (
meas_stat_id        int REFERENCES station( stat_id ),
meas_date           date,
quality             int,
temp_min_5cm        real,
temp_min_2m         real,
temp_avg_2m         real,
temp_max_2m         real,
rel_humid           real,
avg_wind_beauf      int,
max_wind_speed      real,
sun_duration        real,
avg_cloud_cov       real,
precip              real,
avg_air_pressure    real,
PRIMARY KEY (meas_stat_id, meas_date)
);
