#! /bin/bash

# This script will download the opengeodb for Germany from
# http://fa-technik.adfc.de/code/opengeodb/ into the specified output
# directory. A wget Logfile is written to the specified log directory.

# Settings
OUTDIR="${PWD}/Daten"
LOGDIR="${PWD}/Daten"

if which wget 2>/dev/null >/dev/null; then
    wget -O ${OUTDIR}/DE.tab -o ${LOGDIR}/wget_opengeodb.log \
    http://fa-technik.adfc.de/code/opengeodb/DE.tab
    if [ $? != "0" ]; then
        echo "Download failed.";
        false;
        exit;
    fi
else
    echo "wget wasn't found!";
    false;
    exit;
fi

# Remove the sensless lines
sed -i '/151845/d' ${OUTDIR}/DE.tab
sed -i '/151847/d' ${OUTDIR}/DE.tab

# THE END
exit;
