SRC_DIR	= data-import/src/main/java/dbs/guericke
JAVA_FILES = $(SRC_DIR)/DatafileReader.java $(SRC_DIR)/DataImport.java \
			 $(SRC_DIR)/Location.java $(SRC_DIR)/Measurement.java \
			 $(SRC_DIR)/Station.java 
IMPORT_PROG = data-importer-1.0-SNAPSHOT/data-importer-1.0-SNAPSHOT.jar
IMPORT_CMD = java -jar $(IMPORT_PROG)
DB_CREDS = --dburl localhost/guericke \
	       --username erle \
 	       --password halbkugeln

import: import-station import-measurement import-location

import-station: $(IMPORT_PROG)
	psql guericke -c 'TRUNCATE station CASCADE;'
	$(IMPORT_CMD) --statfile ../Daten/wetterdaten_Wetterstation.csv \

import-measurement: $(IMPORT_PROG)
	psql guericke -c 'TRUNCATE measurement CASCADE;'
	$(IMPORT_CMD) --measfile ../Daten/wetterdaten_Wettermessung.csv \

import-location: $(IMPORT_PROG)
	psql guericke -c 'TRUNCATE location CASCADE;'
	psql guericke -c 'TRUNCATE plz CASCADE;'
	$(IMPORT_CMD) --locfile ../Daten/DE.tab  \

$(IMPORT_PROG): $(JAVA_FILES)
	cd data-importer
	mvn package assembly:single
	cd ..
	cp -f data-importer/target/data-importer-1.0-SNAPSHOT-bin.tar.gz .
	tar -xzf data-importer-1.0-SNAPSHOT-bin.tar.gz
	
#psql guericke -f print_all.sql
