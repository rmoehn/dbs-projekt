#!/bin/bash

cd ..
cp -f omnitell/target/guericke-tell-1.0-SNAPSHOT-bin.tar.gz .
tar xzf guericke-tell-1.0-SNAPSHOT-bin.tar.gz
cd guericke-tell-1.0-SNAPSHOT/
java -jar guericke-tell-1.0-SNAPSHOT.jar
