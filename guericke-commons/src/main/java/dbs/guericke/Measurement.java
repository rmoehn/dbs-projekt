package dbs.guericke;

import java.sql.Date;

/**
 * Class representing the weather measurement of a weather station
 */
public class Measurement {
    private final int    measStatId;
    private final Date   date;
    private final int    quality;
    private final float  tempMin5cm;
    private final float  tempMin2m;
    private final float  tempAvg2m;
    private final float  tempMax2m;
    private final float  relHumid;
    private final int    avgWindBeauf;
    private final float  maxWindSpeed;
    private final float  sunDuration;
    private final float  avgCloudCov;
    private final float  precipitation;
    private final float  avgAirPressure;

    /**
     * Creates a new instance of this class with the specified information.
     */
    public Measurement(int measStatId, Date date, int quality,
                       float tempMin5cm, float tempMin2m, float tempAvg2m,
                       float tempMax2m, float relHumid, int avgWindBeauf,
                       float maxWindSpeed, float sunDuration,
                       float avgCloudCov, float precipitation,
                       float avgAirPressure) {
        this.measStatId     = measStatId;
        this.date           = date;
        this.quality        = quality;
        this.tempMin5cm     = tempMin5cm;
        this.tempMin2m      = tempMin2m;
        this.tempAvg2m      = tempAvg2m;
        this.tempMax2m      = tempMax2m;
        this.relHumid       = relHumid;
        this.avgWindBeauf   = avgWindBeauf;
        this.maxWindSpeed   = maxWindSpeed;
        this.sunDuration    = sunDuration;
        this.avgCloudCov    = avgCloudCov;
        this.precipitation  = precipitation;
        this.avgAirPressure = avgAirPressure;
    }

    // Undocumented getters.
    public int    measStatId()     { return measStatId; }
    public Date   date()           { return date; }
    public int    quality()        { return quality; }
    public float  tempMin5cm()     { return tempMin5cm; }
    public float  tempMin2m()      { return tempMin2m; }
    public float  tempAvg2m()      { return tempAvg2m; }
    public float  tempMax2m()      { return tempMax2m; }
    public float  relHumid()       { return relHumid; }
    public int    avgWindBeauf()   { return avgWindBeauf; }
    public float  maxWindSpeed()   { return maxWindSpeed; }
    public float  sunDuration()    { return sunDuration; }
    public float  avgCloudCov()    { return avgCloudCov; }
    public float  precipitation()  { return precipitation; }
    public float  avgAirPressure() { return avgAirPressure; }

    // toString method returning a String representing the fields of this
    // class in the given order space-separated.
    @Override
    public String toString() {
        return String.format("%d ", measStatId) + date.toString() + String
                .format(" %d %.1f %.1f %.1f %.1f %.1f %d %.1f %.1f %.1f %.1f " +
                        "%.1f", quality, tempMin5cm, tempMin2m, tempAvg2m,
                        tempMax2m, relHumid, avgWindBeauf, maxWindSpeed,
                        sunDuration, avgCloudCov, precipitation,
                        avgAirPressure);
    }
}
