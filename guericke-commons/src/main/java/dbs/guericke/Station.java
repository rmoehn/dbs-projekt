package dbs.guericke;

import uk.me.jstott.jcoord.LatLng;

/**
 * Class representing a weather station.
 */
public class Station {
    private final int statId;
    private final String name;
    private final double lat;
    private final double lon;
    private final int alt;
    private final LatLng pos;

    /**
     * Creates a new instance of this class with the specified information.
     */
    public Station(int statId, String name, double lat, double lon, int alt) {
        this.statId = statId;
        this.name   = name;
        this.lat    = lat;
        this.lon    = lon;
        this.alt    = alt;
        this.pos    = new LatLng(lat, lon);
    }

    // Undocumented getters.
    public int    statId() { return statId; }
    public String name()   { return name; }
    public double lat()    { return lat; }
    public double lon()    { return lon; }
    public int    alt()    { return alt; }
    public LatLng pos()    { return pos;  }

    // toString method returning a String representing statId, name, lat,
    // lon and alt in this order space-separated.
    @Override
    public String toString() {
        return String.format("%d %s %.16f %.16f %d", statId, name, lat, lon,
                alt);
    }
}
