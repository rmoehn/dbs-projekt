package dbs.guericke;

import uk.me.jstott.jcoord.LatLng;

/**
 * Class representing a geographic location.
 */
public class Location {
    private final int locId;
    private final String name;
    private final double lat;
    private final double lon;
    private final String[] plzs;
    private final LatLng pos;

    /**
     * Creates a new instance of this class with the specified information.
     */
    public Location(int locId, String name, double lat, double lon,
                    String[] plzs) {
        this.locId = locId;
        this.name  = name;
        this.lat   = lat;
        this.lon   = lon;
        this.plzs  = plzs;
        this.pos   = new LatLng(lat, lon);
    }

    // Undocumented getters.
    public int    locId()  { return locId; }
    public String name()   { return name;  }
    public double lat()    { return lat;   }
    public double lon()    { return lon;   }
    public String[] plzs() { return plzs;  }
    public LatLng pos()    { return pos;  }

    // toString method returning a String representing locId, name, lat,
    // lon and plzs in this order space-separated.
    @Override
    public String toString() {
        return String.format("%d %s %.16f %.16f ", locId, name, lat,
                lon) + plzs.toString();
    }
}
